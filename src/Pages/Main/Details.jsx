import React, { useState, useEffect } from 'react'
import { currentDate } from '../../CustomHooks/currentDate';
import { get } from '../../services'
import Icons from './Icons';

export default function Details({ item }) {

    // console.log(item);

    const defaultTemp = item['main']['temp'] - 273.15
    const temp = String(defaultTemp).slice(0, 3)
    const [clock, setClock] = useState(new Date().toLocaleString());
    // useEffect(() => {
    //     let secTimer = setInterval(() => {
    //         get(`https://api.ipgeolocation.io/timezone?apiKey=a68b79b6a45e4202a2db8e99d9d7665c&lat=${item['coord']['lat']}&long=${item['coord']['lon']}`)
    //             .then(response => {
    //                 let unix_timestamp = response.date_time_unix
    //                 var date = new Date(unix_timestamp * 1000);
    //                 var hours = date.getHours();
    //                 var minutes = "0" + date.getMinutes();
    //                 var seconds = "0" + date.getSeconds();
    //                 var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    //                 setClock(formattedTime);
    //             })
    //     }, 1000)
    //     return () => clearInterval(secTimer);
    // }, [item]);



    // useEffect(() => {
    //     get(`https://api.ipgeolocation.io/timezone?apiKey=a68b79b6a45e4202a2db8e99d9d7665c&lat=${item['coord']['lat']}&long=${item['coord']['lon']}`)
    //         .then(response => {
    //             console.log(response);
    //         })
    // }, [item]);
    

    useEffect(() => {
        let secTimer = setInterval(() => {
            setClock(new Date().toLocaleString())

        }, 1000)

        return () => clearInterval(secTimer);
    }, [clock]);


    // console.log(dt);
    //     let secTimer = setInterval(() => {
    //         get(`https://api.ipgeolocation.io/timezone?apiKey=a68b79b6a45e4202a2db8e99d9d7665c&lat=${item['coord']['lat']}&long=${item['coord']['lon']}`)
    //             .then(response => {
    //                 let unix_timestamp = response.date_time_unix
    //                 var date = new Date(unix_timestamp * 1000);
    //                 var hours = date.getHours();
    //                 var minutes = "0" + date.getMinutes();
    //                 var seconds = "0" + date.getSeconds();
    //                 var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    //                 setClock(formattedTime);
    //             })
    //     }, 1000)

    //     return () => clearInterval(secTimer);
    // }, [item]);

    return (
        <>
            <div className={`card1 ${item['weather']['0']['main']} col-lg-8 col-md-7`}> <small className='logo'>the.weather</small>
                <div className="row col-md-11 px-3 py-4 mt-3 mb-3 back-detail-wallpaper">
                    <h1 className="large-font mr-3">{temp}&#176;C</h1>
                    <div className="d-flex flex-column mr-3 city">
                        {/* <h2 className="mt-3 mb-0">{item.name}</h2> <small>{clock} {currentDate()}</small> */}
                        <h2 className="mt-3 mb-0">{item.name}</h2> <small>{currentDate()}</small>
                    </div>
                    <div className="d-flex flex-column text-center pt-1 icons">
                        <Icons item={item} /> <small>{item['weather']['0']['main']}</small>
                    </div>
                </div>
            </div>
        </>
    )
}
