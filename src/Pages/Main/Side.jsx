import React, { useState } from 'react'
import { tempConverter } from '../../CustomHooks/tempConverter';
import Icons from './Icons';

export default function Side({ item, handleCity }) {

    const defaultTemp = item['main']['temp'] - 273.15
    const temp = String(defaultTemp).slice(0, 3)
    const min = tempConverter(item['main']['temp_min']);
    const max = tempConverter(item['main']['temp_max']);
    const feels = tempConverter(item['main']['feels_like']);
    const [city, setCity] = useState('tehran')
    const handleSubmit = (e) => {
        e.preventDefault()
        handleCity(city)
    }
    return (
        <>
            <div className={`card2 ${item['weather']['0']['main']} col-lg-4 col-md-5`}>
                <h5 className='mobile-v'>the.weather<Icons item={item} style={'mobile-v'} /></h5>
                <div className="row pr-3 position-relative">
                    <form onSubmit={handleSubmit} className='col-11'>
                        <input type="text" name="location" placeholder="Another location" className="mb-5 col-12" value={city} onChange={(e) => setCity(e.target.value)} />
                        <button type='submit' className="fa fa-search mb-5 mr-0 text-center select-btn-btn"></button>
                    </form>
                </div>
                <div className="mr-5">
                    <p className='title-details'>Weather Details</p>
                    <div className="row px-3 mobile-v-temp">
                        <p className="title-text">Temp</p>
                        <p className="ml-auto result-text">{temp}°C</p>
                    </div>
                    <div className="row px-3">
                        <p className="title-text">Feels Like</p>
                        <p className="ml-auto result-text">{feels}°C</p>
                    </div>
                    <div className="row px-3">
                        <p className="title-text">Humidity</p>
                        <p className="ml-auto result-text">{item['main']['humidity']}%</p>
                    </div>
                    <div className="row px-3">
                        <p className="title-text">Pressure</p>
                        <p className="ml-auto result-text">{item['main']['pressure']}mbar</p>
                    </div>
                    <div className="row px-3">
                        <p className="title-text">Wind</p>
                        <p className="ml-auto result-text">{item['wind']['speed']}km/h</p>
                    </div>
                    <div className="row px-3">
                        <p className="title-text">Temp Min</p>
                        <p className="ml-auto result-text">{min}°C</p>
                    </div>
                    <div className="row px-3">
                        <p className="title-text">Temp Max</p>
                        <p className="ml-auto result-text">{max}°C</p>
                    </div>
                    <div className="line mt-3 mb-5"></div>


                </div>
            </div>
        </>
    )
}