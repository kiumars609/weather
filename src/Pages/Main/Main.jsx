import React from 'react'
import Details from './Details'
import Side from './Side'
import Loading from '../../Components/Loading'
import './style.css'
import './styleMobile.css'
import { Backend } from './Backend'

export default function Main() {

    const back = Backend()
    return (
        <>
            {back.loader ? <Loading /> :
                <div className={`main-panel ${back.data['weather']['0']['main']} d-flex align-items-center`}>
                    <div className="container-fluid px-1 px-sm-3 mx-auto">
                        <div className="row d-flex justify-content-center">
                            <div className="row card0">
                                <Details item={back.data} />
                                <Side item={back.data} handleCity={back.handleCity} />
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    )
}
