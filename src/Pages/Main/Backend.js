import { useState, useEffect } from 'react'
import { get } from '../../services'

export function Backend() {
    const [data, setData] = useState([])
    const [dataCity, setDataCity] = useState([])
    const [getCity, setGetCity] = useState('tehran')
    const [loader, setLoader] = useState(true)
    const [loaderCity, setLoaderCity] = useState(true)

    const handleCity = (item) => {
        setGetCity(item)
    }

    useEffect(() => {
        if (getCity !== '') {
            get(`https://api.geoapify.com/v1/geocode/search?text=${getCity}&lang=en&limit=5&format=json&apiKey=8a42b2524eb546ae93468037031a1629`)
                .then(response => {
                    setDataCity(response['results']['0'])
                    setLoaderCity(false)
                })
        }
    }, [getCity]);

    useEffect(() => {
        if (loaderCity === false) {
            get(`https://api.openweathermap.org/data/2.5/weather?lat=${String(dataCity['lat'])}&lon=${String(dataCity['lon'])}&appid=6f89ed549a960df1f999bcaac0420d22`)
                .then(response => {
                    setData(response)
                    setLoader(false)
                })
        }
    }, [dataCity, loaderCity]);

    return { loader, data, handleCity }
}
