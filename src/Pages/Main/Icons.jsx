import React from 'react'
import './style.css'

export default function Icons({ item, style }) {
    const ico = item.weather['0']['icon']

    if (ico === '01d') {
        return <img src="/assets/image/icon/day.svg" width="80px" alt="" className={`m-0 p-0 ${style}`} />
    }
    if (ico === '01n') {
        return <img src="/assets/image/icon/night.svg" width="80px" alt="" className={`m-0 p-0 ${style}`} />
    }
    if (ico === '02d') {
        return <img src="/assets/image/icon/cloudy-day-3.svg" width="80px" alt="" className={`m-0 p-0 ${style}`} />
    }
    if (ico === '02n') {
        return <img src="/assets/image/icon/cloudy-night-3.svg" width="80px" alt="" className={`m-0 p-0 ${style}`} />
    }
    if (ico === '03d') {
        return <img src="/assets/image/icon/cloudy.svg" width="80px" alt="" className={`m-0 p-0 ${style}`} />
    }
    if (ico === '03n') {
        return <img src="/assets/image/icon/cloudy.svg" width="80px" alt="" className={`m-0 p-0 ${style}`} />
    }
    if (ico === '04d') {
        return <img src="/assets/image/icon/cloudy.svg" width="80px" alt="" className={`m-0 p-0 ${style}`} />
    }
    if (ico === '04n') {
        return <img src="/assets/image/icon/cloudy.svg" width="80px" alt="" className={`m-0 p-0 ${style}`} />
    }
    if (ico === '09d') {
        return <img src="/assets/image/icon/rainy-6.svg" width="80px" alt="" className={`m-0 p-0 ${style}`} />
    }
    if (ico === '09n') {
        return <img src="/assets/image/icon/rainy-6.svg" width="80px" alt="" className={`m-0 p-0 ${style}`} />
    }
    if (ico === '10d') {
        return <img src="/assets/image/icon/rainy-3.svg" width="80px" alt="" className={`m-0 p-0 ${style}`} />
    }
    if (ico === '10n') {
        return <img src="/assets/image/icon/rainy-5.svg" width="80px" alt="" className={`m-0 p-0 ${style}`} />
    }
    if (ico === '11d') {
        return <img src="/assets/image/icon/thunder.svg" width="80px" alt="" className={`m-0 p-0 ${style}`} />
    }
    if (ico === '11n') {
        return <img src="/assets/image/icon/thunder.svg" width="80px" alt="" className={`m-0 p-0 ${style}`} />
    }
    if (ico === '13d') {
        return <img src="/assets/image/icon/snowy-3.svg" width="80px" alt="" className={`m-0 p-0 ${style}`} />
    }
    if (ico === '13n') {
        return <img src="/assets/image/icon/snowy-3.svg" width="80px" alt="" className={`m-0 p-0 ${style}`} />
    }
    if (ico === '50d') {
        return <img src="http://openweathermap.org/img/wn/50d@2x.png" width="50px" height="50px" alt="" />
    }
    if (ico === '50n') {
        return <img src="http://openweathermap.org/img/wn/50n@2x.png" width="50px" height="50px" alt="" />
    }
}
