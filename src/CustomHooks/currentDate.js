export function currentDate(separator = ' ') {

    let newDate = new Date()
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();

    if(month === 1){
        month = 'Jan'
    }
    if(month === 2){
        month = 'Feb'
    }
    if(month === 3){
        month = 'Mar'
    }
    if(month === 4){
        month = 'Apr'
    }
    if(month === 5){
        month = 'May'
    }
    if(month === 6){
        month = 'Jun'
    }
    if(month === 7){
        month = 'Jul'
    }
    if(month === 8){
        month = 'Aug'
    }
    if(month === 9){
        month = 'Sep'
    }
    if(month === 10){
        month = 'Oct'
    }
    if(month === 11){
        month = 'Nov'
    }
    if(month === 12){
        month = 'Dec'
    }

    return `${year}${separator}${month < 10 ? `0${month}` : `${month}`}${separator}${date}`
}