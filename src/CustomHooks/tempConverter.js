export function tempConverter(item) {
    const value = item - 273.15
    const temp = String(value).slice(0, 2)
    return temp
}