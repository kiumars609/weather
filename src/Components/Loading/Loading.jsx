import React from 'react'
import svg from './cloudy.svg'
import './style.css'

export default function Loading() {
    return (
        <div className='main-loading col-12 h-100'>
            <img src={svg} className='col-md-2' alt='' />
        </div>
    )
}
