import React, { useEffect, useState } from 'react'
import useDebounce from '../../CustomHooks/useDebounce';
import { Link } from 'react-router-dom';
import { get } from '../../services/HttpClient';
import './style.css'
// import { useTranslation } from 'react-i18next';

export default function Search({ api, inpt }) {
    // const { t, i18n } = useTranslation();
    const [data, setData] = useState([])
    const [search, setSearch] = useState('')
    const [filterSearch, setFilterSearch] = useState([])
    const [showSearch, setShowSearch] = useState(false);
    const debouncedSearchTerm = useDebounce(search, 1500)
    const handleCloseSearch = () => setShowSearch(false);
    const handleShowSearch = () => setShowSearch(true);

    useEffect(() => {
        get(api)
            .then(response => {
                setData(response)
            })
    }, [api]);

    useEffect(() => {
        setFilterSearch(
            data.filter(results => {
                return results.title.toLowerCase().includes(debouncedSearchTerm.toLowerCase())
            })
        )
        if (search !== '') {
            handleShowSearch()
        }
        else {
            handleCloseSearch()
        }
    }, [debouncedSearchTerm, data, search]);

    const mapData = filterSearch.map((row, index) => {
        return (
            <div key={index} className={`row mx-auto mb-2 ${index === 0 && 'first-result'}`}>
                <div className="col-md-3">
                    <Link to={`/product/${row.category}/${row.id}`} className='col-md-12 search-results' onClick={handleCloseSearch}>
                        <img src={`/assets/main/images/products/${row.category}/${row.img1}`} alt={row.titleSmall} className='col-md-12' />
                    </Link>
                </div>
                <div className="col-md-9">
                    <Link to={`/product/${row.category}/${row.id}`} className='col-md-12 search-results' onClick={handleCloseSearch}>{row.title}</Link>
                </div>
                <hr className='col-md-11 mx-auto' />
            </div>
        )
    })

    return (
        <>
            <div className="section-search-container">
                {!inpt ? <input type="text" className="form-control" placeholder='Search' onChange={e => setSearch(e.target.value)} /> : inpt}
                <div className={`col-12 main-results-search ${showSearch ? 'd-block' : 'd-none'}`} id='mains'>
                    <i className='fa fa-close mt-2 text-danger' role="button"></i>
                    {mapData}
                </div>
            </div>
        </>
    )
}